resource "aws_security_group" "rds_sg" {
  name_prefix = "rds-sg"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name      = "WordPress_db_sg"
    Resource  = "WordPress"
    Terraform = true
  }
}

resource "aws_db_subnet_group" "privat" {
  name       = "${var.app_name}_subnet_group"
  subnet_ids = ["${var.private_subnet_1_id}", "${var.private_subnet_2_id}"]

  tags = {
    Name      = "WordPress_subnet_group"
    Resource  = "WordPress"
    Terraform = true
  }
}

resource "aws_db_instance" "rds_instance" {
  identifier             = "wordpress-database-instance"
  allocated_storage      = 20
  engine                 = "mysql"
  engine_version         = "5.7"
  instance_class         = "db.t3.micro"
  db_name                = "${var.app_name}_database"
  username               = "dbuser"
  password               = "Ib66CBo4R0IY"
  parameter_group_name   = "default.mysql5.7"
  skip_final_snapshot    = true
  vpc_security_group_ids = [aws_security_group.rds_sg.id]
  db_subnet_group_name   = aws_db_subnet_group.privat.name

  tags = {
    Name      = "WordPress_database"
    Resource  = "WordPress"
    Terraform = true
  }
}
