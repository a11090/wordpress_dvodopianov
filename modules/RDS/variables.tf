variable "private_subnet_1_id" {
  type = string
}

variable "private_subnet_2_id" {
  type = string
}

variable "app_name" {
  type = string
}

variable "vpc_id" {
  type = string
}