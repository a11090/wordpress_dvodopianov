output "bastion" {
  value = aws_eip.eip_manager.public_ip
}

output "public_url" {
  value = aws_lb.wordpress_lb.dns_name
}
