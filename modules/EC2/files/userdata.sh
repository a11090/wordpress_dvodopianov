#!/bin/bash

# Add ansible repo in apt and install it
sudo apt-add-repository ppa:ansible/ansible
sudo apt update
sudo apt install ansible -y

# Download ansible manifest from S3 bucket and run it
curl -o /opt/main.yaml "${playbook_url}"
cd /opt && sudo ansible-playbook main.yaml

# Download docker compose yaml from S3 bucket and run it
curl -o /opt/docker-compose.yml "${docker-compose_url}"
docker compose -f /opt/docker-compose.yml up -d