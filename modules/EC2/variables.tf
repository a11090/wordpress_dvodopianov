variable "vpc_id" {
  type = string
}

variable "public_subnet_1_id" {
  type = string
}

variable "public_subnet_2_id" {
  type = string
}

variable "private_subnet_1_id" {
  type = string
}

variable "private_subnet_2_id" {
  type = string
}

variable "rds_endpoint" {
  type = string
}

variable "playbook_url" {
  type = string
}

variable "docker-compose_url" {
  type = string
}

variable "app_name" {
  type = string
}

variable "db_user" {
  type = string
}

variable "db_pass" {
  type = string
}

variable "ami" {
  type = string
}