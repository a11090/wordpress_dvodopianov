# Prepair an user data template
data "template_file" "userdata" {
  template = file("../modules/EC2/files/userdata.sh")

  vars = {
    playbook_url       = "${var.playbook_url}"
    rds_endpoint       = "${var.rds_endpoint}"
    db_user            = "${var.db_user}"
    db_pass            = "${var.db_pass}"
    docker-compose_url = "${var.docker-compose_url}"
  }
}

# Create a security group for EC2 instances
resource "aws_security_group" "wordpress_security_group" {
  name_prefix = "wordpress_security_group"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name      = "WordPress_cluster_sg"
    Resource  = "WordPress_EC2"
    Terraform = true
  }
}

# Generate a private TLS key using ED25519 encryption with a length of 2048 bits
resource "tls_private_key" "bastion" {
  algorithm = "ED25519"
  rsa_bits  = 2048
}

# Leverage the TLS private key to generate a SSH public/private key pair
resource "aws_key_pair" "bastion" {
  key_name   = "wordpress_key"
  public_key = tls_private_key.bastion.public_key_openssh

  # when creating, pipe the private key value to the localhost ~/.ssh directory
  provisioner "local-exec" {
    when    = create
    command = "echo '${tls_private_key.bastion.private_key_openssh}' > ~/.ssh/${self.id}.pem; chmod 0400 ~/.ssh/${self.id}.pem"
  }

  # When this resources is destroyed, delete the associated key from the file system
  provisioner "local-exec" {
    when    = destroy
    command = "rm ~/.ssh/${self.id}.pem"
  }
}

resource "aws_key_pair" "codica_key" {
  key_name = "codica_key"
  public_key = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILWGIWLEftb2/+SNvxxSBCLn21T0GKc6i7eaSaPa7OFn ivankarpenko@codica"
}

# Create an elastic ip address
resource "aws_eip" "eip_manager" {
  instance = aws_instance.bastion.id
  vpc      = true

  tags = {
    Name      = "WordPress_eip"
    Resource  = "WordPress_EC2"
    Terraform = true
  }
}

# Create a bastion instance
resource "aws_instance" "bastion" {
  ami             = "${var.ami}"
  key_name        = aws_key_pair.codica_key.key_name
  instance_type   = "t3.micro"
  security_groups = [aws_security_group.wordpress_security_group.id]
  subnet_id       = var.public_subnet_1_id

  depends_on = [
    tls_private_key.bastion
  ]

  tags = {
    Name      = "WordPress_bastion"
    Resource  = "WordPress_EC2"
    Terraform = true
  }
}

# Associate the elastic ip to the bastion instance
resource "aws_eip_association" "eip_assoc" {
  instance_id   = aws_instance.bastion.id
  allocation_id = aws_eip.eip_manager.id
}

# Create a Launch Configuration for EC2 instances
resource "aws_launch_configuration" "wordpress_launch_configuration" {
  name_prefix     = "wordpres_launch_configuration"
  image_id        = "${var.ami}"
  instance_type   = "t3.micro"
  security_groups = [aws_security_group.wordpress_security_group.id]
  user_data       = data.template_file.userdata.rendered
  key_name        = aws_key_pair.codica_key.key_name

  depends_on = [
    tls_private_key.bastion
  ]

  lifecycle {
    create_before_destroy = true
  }
}

# Create an Auto Scaling Group for EC2 instances
resource "aws_autoscaling_group" "wordpress_autoscaling_group" {
  name_prefix          = "wordpress_autoscaling_group"
  launch_configuration = aws_launch_configuration.wordpress_launch_configuration.name
  min_size             = 2
  max_size             = 3
  vpc_zone_identifier  = [var.private_subnet_1_id, var.private_subnet_2_id]
  target_group_arns    = [aws_lb_target_group.wordpress_lb_tg.arn]
  health_check_type    = "ELB"
}

# Create a load balancer to distribute traffic to the EC2 instances
resource "aws_lb" "wordpress_lb" {
  name               = "wordpress-lb"
  load_balancer_type = "application"
  subnets            = [var.public_subnet_1_id, var.public_subnet_2_id]
  security_groups    = [aws_security_group.wordpress_security_group.id]

  tags = {
    Name      = "WordPress_loadbalancer"
    Resource  = "WordPress_EC2"
    Terraform = true
  }
}

resource "aws_lb_listener" "wordpress_lb_listener" {
  load_balancer_arn = aws_lb.wordpress_lb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "404: page not found"
      status_code  = 404
    }
  }
}

resource "aws_lb_listener_rule" "wordpress_lb_listener_rule" {
  listener_arn = aws_lb_listener.wordpress_lb_listener.arn
  priority     = 100

  condition {
    path_pattern {
      values = ["*"]
    }
  }

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.wordpress_lb_tg.arn
  }
}

resource "aws_lb_target_group" "wordpress_lb_tg" {
  name     = "wordpress-asg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = var.vpc_id

  health_check {
    path                = "/"
    protocol            = "HTTP"
    matcher             = "200"
    interval            = "15"
    timeout             = 3
    healthy_threshold   = 2
    unhealthy_threshold = 2
  }

  tags = {
    Name      = "WordPress_lb_target_group"
    Resource  = "WordPress_EC2"
    Terraform = true
  }
}
