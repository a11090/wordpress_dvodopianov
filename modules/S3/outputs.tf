output  "playbook" {
  value = data.local_file.playbook_url.content
}

output  "docker-compose" {
  value = data.local_file.docker-compose_url.content
}