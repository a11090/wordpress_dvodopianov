variable "rds_endpoint" {
  type = string
}

variable "db_user" {
  type = string
}

variable "db_pass" {
  type = string
}