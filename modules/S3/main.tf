data "template_file" "docker-compose" {
  template = file("../modules/S3/files/docker-compose.yml")

  vars = {
    db_host = "${var.rds_endpoint}"
    db_user = "${var.db_user}"
    db_pass = "${var.db_pass}"
  }
}

# Create a bucket
resource "aws_s3_bucket" "artifacts" {
  bucket = "dvodopianov-ansible"

  tags = {
    Name      = "WordPress_ansible_bucket"
    Resource  = "WordPress"
    Terraform = true
  }
}

resource "aws_s3_bucket_acl" "example_bucket_acl" {
  bucket = aws_s3_bucket.artifacts.id
  acl    = "private"
}

# Upload an object
resource "aws_s3_object" "playbook" {
  bucket = aws_s3_bucket.artifacts.id
  key    = "main.yaml"
  acl    = "private"
  source = "../modules/S3/files/main.yaml"
  etag   = filemd5("../modules/S3/files/main.yaml")
}

# Upload an object
resource "aws_s3_object" "docker-compose" {
  bucket  = aws_s3_bucket.artifacts.id
  key     = "docker-compose.yml"
  acl     = "private"
  content = data.template_file.docker-compose.rendered
}

resource "null_resource" "playbook_url" {
  provisioner "local-exec" {
    command = "printf '%s' $(aws s3 presign s3://${aws_s3_bucket.artifacts.id}/${aws_s3_object.playbook.key} --expires-in 40000) > playbook_url"
  }
}

data "local_file" "playbook_url" {
  depends_on = [
    null_resource.playbook_url
  ]
  filename = "playbook_url"
}

resource "null_resource" "docker-compose_url" {
  provisioner "local-exec" {
    command = "printf '%s' $(aws s3 presign s3://${aws_s3_bucket.artifacts.id}/${aws_s3_object.docker-compose.key} --expires-in 40000) > docker-compose_url"
  }
}

data "local_file" "docker-compose_url" {
  depends_on = [
    null_resource.docker-compose_url
  ]
  filename = "docker-compose_url"
}