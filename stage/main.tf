terraform {
  backend "s3" {
    bucket         = "dvodopianov-terraform-backend"
    key            = "terraform.tfstate"
    region         = "eu-central-1"
    dynamodb_table = "dvodopianov-terraform-lock"
    encrypt        = true
  }
}

provider "aws" {
  region = "eu-central-1"
}

locals {
  db_user  = "dbuser"
  db_pass  = "Ib66CBo4R0IY"
  app_name = "wordpress"
  ami      = "ami-0d1ddd83282187d18"
}

module "vpc" {
  source = "../modules/VPC/"
}

module "S3" {
  source = "../modules/S3"

  db_user      = local.db_user
  db_pass      = local.db_pass
  rds_endpoint = module.RDS.rds_endpoint
}

module "RDS" {
  source = "../modules/RDS"

  private_subnet_1_id = module.vpc.privat_subnet_1_id
  private_subnet_2_id = module.vpc.privat_subnet_2_id
  app_name            = local.app_name
  vpc_id              = module.vpc.vpc_id

}

module "EC2" {
  source = "../modules/EC2"

  app_name            = local.app_name
  vpc_id              = module.vpc.vpc_id
  public_subnet_1_id  = module.vpc.public_subnet_1_id
  public_subnet_2_id  = module.vpc.public_subnet_2_id
  private_subnet_1_id = module.vpc.privat_subnet_1_id
  private_subnet_2_id = module.vpc.privat_subnet_2_id
  rds_endpoint        = module.RDS.rds_endpoint
  playbook_url        = module.S3.playbook
  docker-compose_url  = module.S3.docker-compose
  db_user             = local.db_user
  db_pass             = local.db_pass
  depends_on          = [module.S3]
  ami                 = local.ami
}

output "bastion" {
  value = module.EC2.bastion
}

output "public_url" {
  value = module.EC2.public_url
}