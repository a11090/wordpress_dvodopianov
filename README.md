# Terraform template that deploys an WordPress EC2 cluster to AWS

### Store terraform state in S3 bucket

Before starting the deployment, you need to create a bucket and a table in DynamoDB to store your terraform state

```shell
cd ./S3_state
terraform init
terraform apply
cd ../
```

After these steps, you will have a bucket and a table

![Bucket_S3.png](documentation/Bucket_S3.png)

![DynamoDB.png](documentation/DynamoDB.png)

### Run project

```shell
cd ./stage
terraform init
terraform apply
```